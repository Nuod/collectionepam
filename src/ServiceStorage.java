import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class ServiceStorage {


    public List<String> getText(File f) {
        String spliters = ".,! ?\n-*/";
        StringBuilder bufWord = new StringBuilder();
        List<String> text = new ArrayList<String>();
        try (FileReader reader = new FileReader(f)) {
            int c;
            while ((c = reader.read()) != -1) {

                if (spliters.contains(Character.toString((char) c))) {
                    if (bufWord.length() != 0) {
                        text.add(bufWord.toString());
                        bufWord = new StringBuilder();
                    }
                } else {
                    bufWord.append((char) c);
                }
            }
            //System.out.println(text + "\n" + text.size());
            return text;
        } catch (IOException e) {
            System.out.println("Something don't work!");
            return null;
        }
    }

    public StoregeOfTextAndWord CountWord(StoregeOfTextAndWord storege) {
        Map<String, Integer> counter = new HashMap<>();
        List<String> text = storege.getText();
        for (String itr : text) {
            Integer tmpCount = counter.get(itr);
            counter.put(itr, tmpCount == null ? 1 : tmpCount + 1);
        }
        storege.setDictionary(counter);
        return storege;
    }

    public StoregeOfTextAndWord AllUniqueWord(StoregeOfTextAndWord storege) {
        Set<String> uniqueWord = new HashSet<>();
        List<String> text = storege.getText();
        for (String itr : text) {
            uniqueWord.add(itr);
        }
        storege.setUniqueWord(uniqueWord);
        return storege;
    }

    public StoregeOfTextAndWord SortedAllUniqueWord(StoregeOfTextAndWord storege) {
        SortedSet<String> sortedUniqueWord = new TreeSet<>();
        List<String> text = storege.getText();
        for (String itr : text) {
            sortedUniqueWord.add(itr);
        }
        //System.out.println(sortedUniqueWord + "\n\n");
        storege.setSortedUniqueWord(sortedUniqueWord);
        return storege;
    }
}
