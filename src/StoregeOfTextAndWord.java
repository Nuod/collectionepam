import java.util.*;

public class StoregeOfTextAndWord {
    private List<String> text;
    private Map<String, Integer> dictionary;
    private Set<String> uniqueWord;
    private SortedSet<String> sortedUniqueWord;

    @Override
    public String toString() {
        return "StoregeOfTextAndWord{\n" +
                "dictionary=" + dictionary +
                ",\nuniqueWord=" + uniqueWord +
                ",\nsortedUniqueWord=" + sortedUniqueWord +
                '}';
    }

    public StoregeOfTextAndWord() {

    }

    public List<String> getText() {
        return text;
    }

    public void setText(List<String> text) {
        this.text = text;
    }

    public Map<String, Integer> getDictionary() {
        return dictionary;
    }

    public void setDictionary(Map<String, Integer> dictionary) {
        this.dictionary = dictionary;
    }

    public Set<String> getUniqueWord() {
        return uniqueWord;
    }

    public void setUniqueWord(Set<String> uniqueWord) {
        this.uniqueWord = uniqueWord;
    }

    public SortedSet<String> getSortedUniqueWord() {
        return sortedUniqueWord;
    }

    public void setSortedUniqueWord(SortedSet<String> sortedUniqueWord) {
        this.sortedUniqueWord = sortedUniqueWord;
    }
}
